# Spring with Validation

1. Validaciones por medio de clases de tipo **Record**
2. Manejo de excepciones con **RestControllerAdvice**
3. Información de mensajes de error customizado

### Custom Validation

```java
        public final class ParseErrorsAPI {
            private ParseErrorsAPI() {
            }
        
            public static Map<String, Object> parseErrors(String type, List<FieldError> fieldErrors) {
                Map<String, Object> response = new HashMap<>();
                List<String> errors = new ArrayList<>();
                response.put("type", type);
                fieldErrors.forEach(field -> {
                    errors.add(String.format("%s %s", field.getField(), field.getDefaultMessage()));
                });
                response.put("errors", errors);
                return response;
            }
        }

        // ---------------------------------

        @RestControllerAdvice
        public class ValidationRestAdvice {
        
            @ExceptionHandler(MethodArgumentNotValidException.class)
            public ResponseEntity<?> handleValidationException(
                MethodArgumentNotValidException ex
            ) {
                var response = ParseErrorsAPI.parseErrors("validation_parameters", ex.getFieldErrors());
                return ResponseEntity.badRequest().body(response);
            }
        }
 ```
