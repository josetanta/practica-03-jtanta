package org.nttdata.jtanta.demospringvalidation.api.dto.request;


import org.nttdata.jtanta.demospringvalidation.domain.models.Departamento;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

public record DepartamentoCreateRequest(
	@NotBlank
	@Size(
		min = 5,
		max = 20
	)
	String nombre
) {

	public Departamento toModel() {
		return Departamento.builder()
			.nombre(nombre)
			.build();
	}
}
