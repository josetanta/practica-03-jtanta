package org.nttdata.jtanta.demospringvalidation.api.dto.request;


import org.nttdata.jtanta.demospringvalidation.domain.models.Empresa;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

public record EmpresaCreateRequest(
	@NotBlank
	@Size(
		min = 5,
		max = 20
	)
	String nombre
) {

	public Empresa toModel() {
		return Empresa.builder()
			.nombre(nombre)
			.build();
	}
}
