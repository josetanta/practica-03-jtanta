package org.nttdata.jtanta.demospringvalidation.api.rest;

import org.nttdata.jtanta.demospringvalidation.api.dto.request.EmpresaCreateRequest;
import org.nttdata.jtanta.demospringvalidation.domain.service.EmpresaService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

@RestController
@RequestMapping("api/empresas")
@RequiredArgsConstructor
public class EmpresaAPIRest {

	private final EmpresaService empresaService;

	@PostMapping("create")
	public ResponseEntity<?> postCreateEmpresa(
		@Valid @RequestBody EmpresaCreateRequest empresaRequest
	) throws URISyntaxException {
		return ResponseEntity.created(new URI("")).body(empresaService.create(empresaRequest.toModel()));
	}

	@GetMapping
	public ResponseEntity<?> getListEmpresas() {
		return ResponseEntity.ok(empresaService.listObject());
	}
}
