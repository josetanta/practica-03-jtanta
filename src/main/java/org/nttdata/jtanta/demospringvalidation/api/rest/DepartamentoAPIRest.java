package org.nttdata.jtanta.demospringvalidation.api.rest;

import org.nttdata.jtanta.demospringvalidation.api.dto.request.DepartamentoCreateRequest;
import org.nttdata.jtanta.demospringvalidation.domain.service.DepartamentoService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;

@RestController
@RequestMapping("api/departamentos")
@RequiredArgsConstructor
public class DepartamentoAPIRest {

	private final DepartamentoService departamentoService;

	@PostMapping("create")
	public ResponseEntity<?> postCreateDepartamento(
		@Valid @RequestBody DepartamentoCreateRequest createRequest
	) {
		var response = departamentoService.create(createRequest.toModel());
		return ResponseEntity.created(URI.create("")).body(response);
	}

	@GetMapping
	public ResponseEntity<?> getListDepartamentos() {
		return ResponseEntity.ok(departamentoService.listObject());
	}

}
