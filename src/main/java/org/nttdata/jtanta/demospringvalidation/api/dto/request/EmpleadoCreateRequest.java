package org.nttdata.jtanta.demospringvalidation.api.dto.request;


import org.nttdata.jtanta.demospringvalidation.domain.models.Departamento;
import org.nttdata.jtanta.demospringvalidation.domain.models.Empleado;
import org.nttdata.jtanta.demospringvalidation.domain.models.Empresa;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

public record EmpleadoCreateRequest(
	@NotBlank
	@Size(min = 5)
	String nombres,

	@NotBlank
	@Size(min = 5)
	String apellidos,

	@NotBlank
	@Size(min = 8, max = 8, message = "debe contener 8 dígitos")
	String dni,

	@NotNull(message = "debe tener un valor")
	@Min(value = 1, message = "no admite valores menores a 1")
	Long departamentoId,

	@NotNull(message = "debe tener un valor")
	@Min(value = 1, message = "no admite valores menores a 1")
	Integer empresaId
) {

	public Empleado toModel() {
		return Empleado.builder()
			.nombres(nombres)
			.apellidos(apellidos)
			.dni(dni)
			.empresa(Empresa.builder()
				.id(empresaId)
				.build())
			.departamento(Departamento.builder()
				.id(departamentoId)
				.build())
			.build();
	}
}
