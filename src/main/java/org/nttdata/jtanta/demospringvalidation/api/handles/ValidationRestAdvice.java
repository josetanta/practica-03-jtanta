package org.nttdata.jtanta.demospringvalidation.api.handles;

import org.nttdata.jtanta.demospringvalidation.utils.ParseErrorsAPI;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

@RestControllerAdvice
public class ValidationRestAdvice {

	@ExceptionHandler(MethodArgumentNotValidException.class)
	public ResponseEntity<?> handleValidationException(
		MethodArgumentNotValidException ex
	) {
		var response = ParseErrorsAPI.parseErrors("validation_parameters", ex.getFieldErrors());
		return ResponseEntity.badRequest().body(response);
	}
}
