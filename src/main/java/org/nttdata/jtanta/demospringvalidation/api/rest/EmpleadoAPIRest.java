package org.nttdata.jtanta.demospringvalidation.api.rest;

import org.nttdata.jtanta.demospringvalidation.api.dto.request.EmpleadoCreateRequest;
import org.nttdata.jtanta.demospringvalidation.domain.service.EmpleadoService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;

@RestController
@RequestMapping("api/empleados")
@RequiredArgsConstructor
public class EmpleadoAPIRest {

	private final EmpleadoService empleadoService;

	@PostMapping("create")
	public ResponseEntity<?> postCreateEmpleado(
		@Valid @RequestBody EmpleadoCreateRequest createRequest
	) {
		var response = empleadoService.create(createRequest.toModel());
		return ResponseEntity.created(URI.create("")).body(response);
	}

	@GetMapping(params = {"empleado_id"})
	public ResponseEntity<?> getSingleEmpleado(
		@RequestParam("empleado_id") Long id
	) {
		var response = empleadoService.single(id);
		return ResponseEntity.created(URI.create("")).body(response);
	}

	@GetMapping
	public ResponseEntity<?> getListEmpleados() {
		return ResponseEntity.ok(empleadoService.listObject());
	}
}
