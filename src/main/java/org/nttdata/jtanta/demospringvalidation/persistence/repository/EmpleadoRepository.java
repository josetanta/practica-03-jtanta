package org.nttdata.jtanta.demospringvalidation.persistence.repository;

import org.nttdata.jtanta.demospringvalidation.domain.models.Empleado;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface EmpleadoRepository extends PagingAndSortingRepository<Empleado, Long> {
}
