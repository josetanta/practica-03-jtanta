package org.nttdata.jtanta.demospringvalidation.persistence.repository;

import org.nttdata.jtanta.demospringvalidation.domain.models.Departamento;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface DepartamentoRepository extends PagingAndSortingRepository<Departamento, Long> {
}
