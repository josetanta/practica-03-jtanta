package org.nttdata.jtanta.demospringvalidation.persistence.repository;

import org.nttdata.jtanta.demospringvalidation.domain.models.Empresa;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface EmpresaRepository extends PagingAndSortingRepository<Empresa, Integer> {
}
