package org.nttdata.jtanta.demospringvalidation.domain.service;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;

public interface CrudService<M, ID> {
	M create(M m);

	M single(ID id);

	List<M> listObject();

	void update(M m);

	void delete(ID id);

	default Page<M> listPage(Pageable pageable) {
		return null;
	}
}
