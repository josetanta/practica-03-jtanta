package org.nttdata.jtanta.demospringvalidation.domain.service.adapters;

import org.nttdata.jtanta.demospringvalidation.domain.models.Departamento;
import org.nttdata.jtanta.demospringvalidation.domain.service.DepartamentoService;
import org.nttdata.jtanta.demospringvalidation.persistence.repository.DepartamentoRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class DepartamentoServiceAdapter implements DepartamentoService {

	private final DepartamentoRepository departamentoRepository;

	@Override
	public Departamento create(Departamento departamento) {
		return departamentoRepository.save(departamento);
	}

	@Override
	public Departamento single(Long id) {
		return departamentoRepository.findById(id)
			.orElseThrow();
	}

	@Override
	public List<Departamento> listObject() {
		return (List<Departamento>) departamentoRepository.findAll();
	}

	@Override
	public void update(Departamento departamento) {
		var dep = single(departamento.getId());
		dep.setNombre(departamento.getNombre());
		departamentoRepository.save(dep);
	}

	@Override
	public void delete(Long id) {
		var dep = single(id);
		departamentoRepository.delete(dep);
	}
}
