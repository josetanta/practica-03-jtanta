package org.nttdata.jtanta.demospringvalidation.domain.service;

import org.nttdata.jtanta.demospringvalidation.domain.models.Empleado;

public interface EmpleadoService extends CrudService<Empleado, Long> {
	// todo: can add any methods
}
