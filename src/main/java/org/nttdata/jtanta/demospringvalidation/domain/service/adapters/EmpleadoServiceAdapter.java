package org.nttdata.jtanta.demospringvalidation.domain.service.adapters;

import org.nttdata.jtanta.demospringvalidation.domain.models.Empleado;
import org.nttdata.jtanta.demospringvalidation.domain.service.EmpleadoService;
import org.nttdata.jtanta.demospringvalidation.persistence.repository.EmpleadoRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class EmpleadoServiceAdapter implements EmpleadoService {

	private final EmpleadoRepository empleadoRepository;

	@Override
	public Empleado create(Empleado empleado) {
		return empleadoRepository.save(empleado);
	}

	@Override
	public Empleado single(Long id) {
		return empleadoRepository.findById(id)
			.orElseThrow();
	}

	@Override
	public List<Empleado> listObject() {
		return (List<Empleado>) empleadoRepository.findAll();
	}

	@Override
	public void update(Empleado empleado) {
		var getEmpleado = single(empleado.getId());
		getEmpleado.setDepartamento(empleado.getDepartamento());
		getEmpleado.setEmpresa(empleado.getEmpresa());
		getEmpleado.setNombres(empleado.getNombres());
		getEmpleado.setApellidos(empleado.getApellidos());
		empleadoRepository.save(getEmpleado);
	}

	@Override
	public void delete(Long id) {
		var getEmpleado = single(id);
		empleadoRepository.delete(getEmpleado);
	}
}
