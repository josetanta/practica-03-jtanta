package org.nttdata.jtanta.demospringvalidation.domain.models;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "empleados")
public class Empleado {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	private String nombres;
	private String apellidos;

	@Column(unique = true, length = 8)
	private String dni;

	@ManyToOne
	@JoinColumn(name = "departamento_id")
	private Departamento departamento;

	@ManyToOne
	@JoinColumn(name = "empresa_id")
	private Empresa empresa;
}
