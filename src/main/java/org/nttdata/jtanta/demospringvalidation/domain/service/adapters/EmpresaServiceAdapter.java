package org.nttdata.jtanta.demospringvalidation.domain.service.adapters;

import org.nttdata.jtanta.demospringvalidation.domain.models.Empresa;
import org.nttdata.jtanta.demospringvalidation.domain.service.EmpresaService;
import org.nttdata.jtanta.demospringvalidation.persistence.repository.EmpresaRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class EmpresaServiceAdapter implements EmpresaService {

	private final EmpresaRepository empresaRepository;

	@Override
	public Empresa create(Empresa empresa) {
		return empresaRepository.save(empresa);
	}

	@Override
	public Empresa single(Integer id) {
		return empresaRepository.findById(id)
			.orElseThrow();
	}

	@Override
	public List<Empresa> listObject() {
		return (List<Empresa>) empresaRepository.findAll();
	}

	@Override
	public void update(Empresa empresa) {
		var getEmpresa = single(empresa.getId());
		getEmpresa.setNombre(empresa.getNombre());
		empresaRepository.save(getEmpresa);
	}

	@Override
	public void delete(Integer id) {
		var getEmpresa = single(id);
		empresaRepository.delete(getEmpresa);
	}
}
