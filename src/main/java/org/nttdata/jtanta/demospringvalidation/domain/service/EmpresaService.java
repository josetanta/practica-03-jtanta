package org.nttdata.jtanta.demospringvalidation.domain.service;

import org.nttdata.jtanta.demospringvalidation.domain.models.Empresa;

public interface EmpresaService extends CrudService<Empresa, Integer> {
	// todo: can add any methods
}
