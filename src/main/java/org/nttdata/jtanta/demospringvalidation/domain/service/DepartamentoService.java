package org.nttdata.jtanta.demospringvalidation.domain.service;

import org.nttdata.jtanta.demospringvalidation.domain.models.Departamento;

public interface DepartamentoService extends CrudService<Departamento, Long> {
	// todo: can add any methods
}
