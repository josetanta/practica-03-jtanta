package org.nttdata.jtanta.demospringvalidation.utils;

import org.springframework.validation.FieldError;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public final class ParseErrorsAPI {
	private ParseErrorsAPI() {
	}

	public static Map<String, Object> parseErrors(String type, List<FieldError> fieldErrors) {
		Map<String, Object> response = new HashMap<>();
		List<String> errors = new ArrayList<>();
		response.put("type", type);
		fieldErrors.forEach(field -> {
			errors.add(String.format("%s %s", field.getField(), field.getDefaultMessage()));
		});
		response.put("errors", errors);
		return response;
	}
}
